# NFS

## NFS サーバの準備

### 通常のマシンに構築する場合

```
# インストール
## Ubuntu の場合
sudo apt install nfs-kernel-server

# 設定
sudo mkdir -p /exports
sudo tee /etc/exports <<EOF
/exports *(rw,all_squash,async,anonuid=1000,anongid=1000) 
EOF

# 再起動
sudo systemctl restart nfs-server

# サーバの IP
DEVNAME=enp3s0

export NFSSV_IP=$(ip -4 addr show dev ${DEVNAME} | grep inet | sed -E "s/.*inet ([^/]+).*/\1/g")
echo ${NFSSV_IP}
```

### クラスタ内に構築する場合

クライアント側との同じクラスタに構築するとクラスタがダウンしたときにどうなるか分からないため非推奨。

```
kubectl label nodes kind-worker nfssv=true

kubectl apply -f nfs-server.yml

# サーバの IP
export NFSSV_IP=$(kubectl get services/nfs-server -o go-template='{{.spec.clusterIP}}')
echo ${NFSSV_IP}
```

## PV / PVC の作成

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: ${NFSSV_IP}
    path: "/"
EOF

kubectl apply -f nfs-pvc.yml
```
