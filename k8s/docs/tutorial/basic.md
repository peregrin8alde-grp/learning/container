# 基本操作

https://kubernetes.io/docs/tutorials/kubernetes-basics/

## Create a Cluster

練習用のクラスタは kind で生成

```
kind create cluster --name basic

kind delete cluster --name basic
```

```
$ kubectl cluster-info --context kind-basic
Kubernetes control plane is running at https://127.0.0.1:38975
CoreDNS is running at https://127.0.0.1:38975/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

$ kubectl get nodes
NAME                  STATUS   ROLES                  AGE    VERSION
basic-control-plane   Ready    control-plane,master   2m7s   v1.21.1
```

## Deploy an App

https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/

```
kubectl get nodes --help > help-get-nodes.txt
```

```
kubectl create deployment --help > help-create-deployment.txt

kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
```

```
$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   0/1     1            0           10s
```

```
$ kubectl proxy
Starting to serve on 127.0.0.1:8001
```

```
$ curl http://localhost:8001/version
{
  "major": "1",
  "minor": "21",
  "gitVersion": "v1.21.1",
  "gitCommit": "5e58841cce77d4bc13713ad2b91fa0d961e69192",
  "gitTreeState": "clean",
  "buildDate": "2021-05-18T01:10:20Z",
  "goVersion": "go1.16.4",
  "compiler": "gc",
  "platform": "linux/amd64"

$ export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
$ echo Name of the Pod: $POD_NAME
Name of the Pod: kubernetes-bootcamp-57978f5f5d-mlcqk
```

## Explore Your App

- kubectl get - list resources
- kubectl describe - show detailed information about a resource
- kubectl logs - print the logs from a container in a pod
- kubectl exec - execute a command on a container in a pod

```
$ kubectl get pods
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-mlcqk   1/1     Running   3          47h

$ kubectl describe pods
Name:         kubernetes-bootcamp-57978f5f5d-mlcqk
Namespace:    default
Priority:     0
Node:         basic-control-plane/172.23.0.2
Start Time:   Mon, 14 Jun 2021 19:31:04 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=57978f5f5d
Annotations:  <none>
Status:       Running
IP:           10.244.0.5
IPs:
  IP:           10.244.0.5
Controlled By:  ReplicaSet/kubernetes-bootcamp-57978f5f5d
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://d719e54699ab6475990c7bc23b23c54d92d2dc9fe934319f088dbe7660506741
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Wed, 16 Jun 2021 17:43:58 +0900
    Last State:     Terminated
      Reason:       Unknown
      Exit Code:    255
      Started:      Tue, 15 Jun 2021 18:49:32 +0900
      Finished:     Wed, 16 Jun 2021 17:43:38 +0900
    Ready:          True
    Restart Count:  3
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-8brs4 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-8brs4:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:                      <none>

$ curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {
    
  },
  "status": "Failure",
  "message": "error trying to reach service: dial tcp 10.244.0.5:80: connect: connection refused",
  "reason": "ServiceUnavailable",
  "code": 503

$ kubectl logs $POD_NAME
Kubernetes Bootcamp App Started At: 2021-06-16T08:43:58.913Z | Running On:  kubernetes-bootcamp-57978f5f5d-mlcqk

$ kubectl exec $POD_NAME -- env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=kubernetes-bootcamp-57978f5f5d-mlcqk
NPM_CONFIG_LOGLEVEL=info
NODE_VERSION=6.3.1
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
KUBERNETES_SERVICE_HOST=10.96.0.1
KUBERNETES_SERVICE_PORT=443
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_PORT=443
HOME=/root
```

```
kubectl exec -ti $POD_NAME -- bash
```

```
cat server.js

curl localhost:8080

exit
```

## Expose Your App Publicly

```
$ kubectl get pods
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-mlcqk   1/1     Running   5          3d

$ kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   3d

$ kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
amp exposedservice/kubernetes-bootcamp exposed

$ kubectl get services
NAME                  TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes            ClusterIP   10.96.0.1      <none>        443/TCP          3d
kubernetes-bootcamp   NodePort    10.96.173.74   <none>        8080:30182/TCP   41s

$ kubectl describe services/kubernetes-bootcamp
Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.96.173.74
IPs:                      10.96.173.74
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  30182/TCP
Endpoints:                10.244.0.4:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

$ export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
$ echo NODE_PORT=$NODE_PORT
NODE_PORT=30182

### kind だとコンテナのためここでポートが外部向けには開かない
### $ curl $(minikube ip):$NODE_PORT
docker exec -it basic-control-plane curl localhost:$NODE_PORT

$ kubectl describe deployment
Name:                   kubernetes-bootcamp
Namespace:              default
CreationTimestamp:      Mon, 14 Jun 2021 19:31:04 +0900
Labels:                 app=kubernetes-bootcamp
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=kubernetes-bootcamp
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=kubernetes-bootcamp
  Containers:
   kubernetes-bootcamp:
    Image:        gcr.io/google-samples/kubernetes-bootcamp:v1
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   kubernetes-bootcamp-57978f5f5d (1/1 replicas created)
Events:          <none>

$ kubectl get pods -l app=kubernetes-bootcamp
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-mlcqk   1/1     Running   6          3d

$ kubectl get services -l app=kubernetes-bootcamp
NAME                  TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes-bootcamp   NodePort   10.96.173.74   <none>        8080:30182/TCP   24m

$ export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
$ echo Name of the Pod: $POD_NAME
Name of the Pod: kubernetes-bootcamp-57978f5f5d-mlcqk

$ kubectl label pod $POD_NAME version=v1
pod/kubernetes-bootcamp-57978f5f5d-mlcqk labeled

$ kubectl describe pods $POD_NAME
Name:         kubernetes-bootcamp-57978f5f5d-mlcqk
Namespace:    default
Priority:     0
Node:         basic-control-plane/172.23.0.2
Start Time:   Mon, 14 Jun 2021 19:31:04 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=57978f5f5d
              version=v1
Annotations:  <none>
Status:       Running
IP:           10.244.0.2
IPs:
  IP:           10.244.0.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-57978f5f5d
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://8662bf542991d983be6ee475b9a2093b74208bd4698a7eed24f84d623fdb060b
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Thu, 17 Jun 2021 20:08:57 +0900
    Last State:     Terminated
      Reason:       Unknown
      Exit Code:    255
      Started:      Thu, 17 Jun 2021 19:42:01 +0900
      Finished:     Thu, 17 Jun 2021 20:08:31 +0900
    Ready:          True
    Restart Count:  6
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-8brs4 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-8brs4:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason          Age   From     Message
  ----    ------          ----  ----     -------
  Normal  SandboxChanged  38m   kubelet  Pod sandbox changed, it will be killed and re-created.
  Normal  Pulled          38m   kubelet  Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created         38m   kubelet  Created container kubernetes-bootcamp
  Normal  Started         38m   kubelet  Started container kubernetes-bootcamp
  Normal  SandboxChanged  11m   kubelet  Pod sandbox changed, it will be killed and re-created.
  Normal  Pulled          11m   kubelet  Container image "gcr.io/google-samples/kubernetes-bootcamp:v1" already present on machine
  Normal  Created         11m   kubelet  Created container kubernetes-bootcamp
  Normal  Started         11m   kubelet  Started container kubernetes-bootcamp

$ kubectl get pods -l version=v1
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-mlcqk   1/1     Running   6          3d


$ kubectl delete service -l app=kubernetes-bootcamp
service "kubernetes-bootcamp" deleted

$ kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   3d

### kind だとコンテナのため元々ポートが外部向けには開かない
### $ curl $(minikube ip):$NODE_PORT
docker exec -it basic-control-plane curl localhost:$NODE_PORT

$ kubectl exec -ti $POD_NAME -- curl localhost:8080
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-57978f5f5d-mlcqk | v=1
```

## Scale Your App

worker を増やしたクラスタを kind で生成

```
kind create cluster --name scale --config kind-scale-config.yaml

kubectl cluster-info --context kind-scale
```

```
$ kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1

$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           24s

$ kubectl get rs
NAME                             DESIRED   CURRENT   READY   AGE
kubernetes-bootcamp-57978f5f5d   1         1         1       66s

$ kubectl scale deployments/kubernetes-bootcamp --replicas=4
deployment.apps/kubernetes-bootcamp scaled

$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   4/4     4            4           2m21s

$ kubectl get pods -o wide
NAME                                   READY   STATUS    RESTARTS   AGE    IP           NODE            NOMINATED NODE   READINESS GATES
kubernetes-bootcamp-57978f5f5d-4k4p7   1/1     Running   0          3m1s   10.244.1.2   scale-worker    <none>           <none>
kubernetes-bootcamp-57978f5f5d-bmnm4   1/1     Running   0          70s    10.244.3.2   scale-worker2   <none>           <none>
kubernetes-bootcamp-57978f5f5d-nsrgx   1/1     Running   0          70s    10.244.4.2   scale-worker4   <none>           <none>
kubernetes-bootcamp-57978f5f5d-pg2zj   1/1     Running   0          70s    10.244.2.2   scale-worker3   <none>           <none>

$ kubectl describe deployments/kubernetes-bootcamp
Name:                   kubernetes-bootcamp
Namespace:              default
CreationTimestamp:      Sat, 19 Jun 2021 15:37:01 +0900
Labels:                 app=kubernetes-bootcamp
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=kubernetes-bootcamp
Replicas:               4 desired | 4 updated | 4 total | 4 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=kubernetes-bootcamp
  Containers:
   kubernetes-bootcamp:
    Image:        gcr.io/google-samples/kubernetes-bootcamp:v1
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Progressing    True    NewReplicaSetAvailable
  Available      True    MinimumReplicasAvailable
OldReplicaSets:  <none>
NewReplicaSet:   kubernetes-bootcamp-57978f5f5d (4/4 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  3m41s  deployment-controller  Scaled up replica set kubernetes-bootcamp-57978f5f5d to 1
  Normal  ScalingReplicaSet  110s   deployment-controller  Scaled up replica set kubernetes-bootcamp-57978f5f5d to 4

$ kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
$ kubectl describe services/kubernetes-bootcamp
Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.96.176.53
IPs:                      10.96.176.53
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  32477/TCP
Endpoints:                10.244.1.2:8080,10.244.2.2:8080,10.244.3.2:8080 + 1 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

$ export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
$ echo NODE_PORT=$NODE_PORT
NODE_PORT=32477

### kind だとコンテナのためここでポートが外部向けには開かない
### $ curl $(minikube ip):$NODE_PORT
$ docker exec -it scale-control-plane curl localhost:$NODE_PORT
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-57978f5f5d-bmnm4 | v=1

$ kubectl scale deployments/kubernetes-bootcamp --replicas=2

$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   2/2     2            2           8m26s

$ kubectl get pods -o wide
NAME                                   READY   STATUS    RESTARTS   AGE     IP           NODE            NOMINATED NODE   READINESS GATES
kubernetes-bootcamp-57978f5f5d-4k4p7   1/1     Running   0          8m51s   10.244.1.2   scale-worker    <none>           <none>
kubernetes-bootcamp-57978f5f5d-nsrgx   1/1     Running   0          7m      10.244.4.2   scale-worker4   <none>           <none>
```

## Update Your App

```
$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   2/2     2            2           11m

$ kubectl get pods
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-4k4p7   1/1     Running   0          11m
kubernetes-bootcamp-57978f5f5d-nsrgx   1/1     Running   0          9m35s

$ kubectl describe pods
Name:         kubernetes-bootcamp-57978f5f5d-4k4p7
Namespace:    default
Priority:     0
Node:         scale-worker/172.23.0.4
Start Time:   Sat, 19 Jun 2021 15:37:01 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=57978f5f5d
Annotations:  <none>
Status:       Running
IP:           10.244.1.2
IPs:
  IP:           10.244.1.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-57978f5f5d
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://7004e7d42e9ae2fcd4d6a93457bb4c1e8ed1955007e47e58ef0a9d09e9657e58
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:37:17 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-kgkvq (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-kgkvq:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  12m   default-scheduler  Successfully assigned default/kubernetes-bootcamp-57978f5f5d-4k4p7 to scale-worker
  Normal  Pulling    12m   kubelet            Pulling image "gcr.io/google-samples/kubernetes-bootcamp:v1"
  Normal  Pulled     11m   kubelet            Successfully pulled image "gcr.io/google-samples/kubernetes-bootcamp:v1" in 14.977009892s
  Normal  Created    11m   kubelet            Created container kubernetes-bootcamp
  Normal  Started    11m   kubelet            Started container kubernetes-bootcamp


Name:         kubernetes-bootcamp-57978f5f5d-nsrgx
Namespace:    default
Priority:     0
Node:         scale-worker4/172.23.0.2
Start Time:   Sat, 19 Jun 2021 15:38:52 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=57978f5f5d
Annotations:  <none>
Status:       Running
IP:           10.244.4.2
IPs:
  IP:           10.244.4.2
Controlled By:  ReplicaSet/kubernetes-bootcamp-57978f5f5d
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://ce4ea568754f4c66448b20939760fbd009da9b889242d7f73f26f97e2777fded
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v1
    Image ID:       gcr.io/google-samples/kubernetes-bootcamp@sha256:0d6b8ee63bb57c5f5b6156f446b3bc3b3c143d233037f3a2f00e279c8fcc64af
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:39:21 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-fcc7x (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-fcc7x:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  10m    default-scheduler  Successfully assigned default/kubernetes-bootcamp-57978f5f5d-nsrgx to scale-worker4
  Normal  Pulling    10m    kubelet            Pulling image "gcr.io/google-samples/kubernetes-bootcamp:v1"
  Normal  Pulled     9m44s  kubelet            Successfully pulled image "gcr.io/google-samples/kubernetes-bootcamp:v1" in 27.855904033s
  Normal  Created    9m44s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    9m44s  kubelet            Started container kubernetes-bootcamp

$ kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2
deployment.apps/kubernetes-bootcamp image updated

$ kubectl get pods
NAME                                   READY   STATUS        RESTARTS   AGE
kubernetes-bootcamp-57978f5f5d-4k4p7   1/1     Terminating   0          13m
kubernetes-bootcamp-57978f5f5d-nsrgx   1/1     Terminating   0          11m
kubernetes-bootcamp-769746fd4-47lkw    1/1     Running       0          24s
kubernetes-bootcamp-769746fd4-d47xm    1/1     Running       0          17s

$ kubectl describe services/kubernetes-bootcamp
Name:                     kubernetes-bootcamp
Namespace:                default
Labels:                   app=kubernetes-bootcamp
Annotations:              <none>
Selector:                 app=kubernetes-bootcamp
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.96.176.53
IPs:                      10.96.176.53
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  32477/TCP
Endpoints:                10.244.2.3:8080,10.244.3.3:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

$ export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
$ echo NODE_PORT=$NODE_PORT
NODE_PORT=32477

### kind だとコンテナのためここでポートが外部向けには開かない
### $ curl $(minikube ip):$NODE_PORT
$ docker exec -it scale-control-plane curl localhost:$NODE_PORT
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-769746fd4-47lkw | v=2

$ kubectl rollout status deployments/kubernetes-bootcamp
deployment "kubernetes-bootcamp" successfully rolled out

$ kubectl describe pods
Name:         kubernetes-bootcamp-769746fd4-47lkw
Namespace:    default
Priority:     0
Node:         scale-worker3/172.23.0.6
Start Time:   Sat, 19 Jun 2021 15:49:47 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.2.3
IPs:
  IP:           10.244.2.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://225619fa56d7d923d5de8ae420dfa362e30c821a8ab9a37b344d2e18c2af94e5
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:49:53 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-7zns2 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-7zns2:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  3m41s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-47lkw to scale-worker3
  Normal  Pulling    3m40s  kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     3m35s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 4.947566071s
  Normal  Created    3m35s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    3m35s  kubelet            Started container kubernetes-bootcamp


Name:         kubernetes-bootcamp-769746fd4-d47xm
Namespace:    default
Priority:     0
Node:         scale-worker2/172.23.0.3
Start Time:   Sat, 19 Jun 2021 15:49:54 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.3.3
IPs:
  IP:           10.244.3.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://c4514eb44a890e61e12eacaedbdc6456608bf055eb834405d67f102493b4974b
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:50:00 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-tlwpw (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-tlwpw:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  3m34s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-d47xm to scale-worker2
  Normal  Pulling    3m34s  kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     3m28s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 5.526844415s
  Normal  Created    3m28s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    3m28s  kubelet            Started container kubernetes-bootcamp

$ kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=gcr.io/google-samples/kubernetes-bootcamp:v10
deployment.apps/kubernetes-bootcamp image updated

$ kubectl get deployments
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   2/2     1            2           17m

$ kubectl get pods
NAME                                  READY   STATUS             RESTARTS   AGE
kubernetes-bootcamp-597654dbd-bqw77   0/1     ImagePullBackOff   0          53s
kubernetes-bootcamp-769746fd4-47lkw   1/1     Running            0          5m14s
kubernetes-bootcamp-769746fd4-d47xm   1/1     Running            0          5m7s

$ kubectl describe pods
Name:         kubernetes-bootcamp-597654dbd-bqw77
Namespace:    default
Priority:     0
Node:         scale-worker4/172.23.0.2
Start Time:   Sat, 19 Jun 2021 15:54:08 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=597654dbd
Annotations:  <none>
Status:       Pending
IP:           10.244.4.3
IPs:
  IP:           10.244.4.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-597654dbd
Containers:
  kubernetes-bootcamp:
    Container ID:   
    Image:          gcr.io/google-samples/kubernetes-bootcamp:v10
    Image ID:       
    Port:           <none>
    Host Port:      <none>
    State:          Waiting
      Reason:       ImagePullBackOff
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-h7t9f (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  kube-api-access-h7t9f:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                 From               Message
  ----     ------     ----                ----               -------
  Normal   Scheduled  101s                default-scheduler  Successfully assigned default/kubernetes-bootcamp-597654dbd-bqw77 to scale-worker4
  Normal   BackOff    22s (x5 over 99s)   kubelet            Back-off pulling image "gcr.io/google-samples/kubernetes-bootcamp:v10"
  Warning  Failed     22s (x5 over 99s)   kubelet            Error: ImagePullBackOff
  Normal   Pulling    11s (x4 over 101s)  kubelet            Pulling image "gcr.io/google-samples/kubernetes-bootcamp:v10"
  Warning  Failed     10s (x4 over 100s)  kubelet            Failed to pull image "gcr.io/google-samples/kubernetes-bootcamp:v10": rpc error: code = NotFound desc = failed to pull and unpack image "gcr.io/google-samples/kubernetes-bootcamp:v10": failed to resolve reference "gcr.io/google-samples/kubernetes-bootcamp:v10": gcr.io/google-samples/kubernetes-bootcamp:v10: not found
  Warning  Failed     10s (x4 over 100s)  kubelet            Error: ErrImagePull


Name:         kubernetes-bootcamp-769746fd4-47lkw
Namespace:    default
Priority:     0
Node:         scale-worker3/172.23.0.6
Start Time:   Sat, 19 Jun 2021 15:49:47 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.2.3
IPs:
  IP:           10.244.2.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://225619fa56d7d923d5de8ae420dfa362e30c821a8ab9a37b344d2e18c2af94e5
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:49:53 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-7zns2 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-7zns2:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  6m2s   default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-47lkw to scale-worker3
  Normal  Pulling    6m1s   kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     5m56s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 4.947566071s
  Normal  Created    5m56s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    5m56s  kubelet            Started container kubernetes-bootcamp


Name:         kubernetes-bootcamp-769746fd4-d47xm
Namespace:    default
Priority:     0
Node:         scale-worker2/172.23.0.3
Start Time:   Sat, 19 Jun 2021 15:49:54 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.3.3
IPs:
  IP:           10.244.3.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://c4514eb44a890e61e12eacaedbdc6456608bf055eb834405d67f102493b4974b
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:50:00 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-tlwpw (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-tlwpw:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  5m55s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-d47xm to scale-worker2
  Normal  Pulling    5m55s  kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     5m49s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 5.526844415s
  Normal  Created    5m49s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    5m49s  kubelet            Started container kubernetes-bootcamp

$ kubectl rollout undo deployments/kubernetes-bootcamp
deployment.apps/kubernetes-bootcamp rolled back

$ kubectl get pods
NAME                                  READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-769746fd4-47lkw   1/1     Running   0          7m5s
kubernetes-bootcamp-769746fd4-d47xm   1/1     Running   0          6m58s

$ kubectl describe pods
Name:         kubernetes-bootcamp-769746fd4-47lkw
Namespace:    default
Priority:     0
Node:         scale-worker3/172.23.0.6
Start Time:   Sat, 19 Jun 2021 15:49:47 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.2.3
IPs:
  IP:           10.244.2.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://225619fa56d7d923d5de8ae420dfa362e30c821a8ab9a37b344d2e18c2af94e5
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:49:53 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-7zns2 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-7zns2:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  7m30s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-47lkw to scale-worker3
  Normal  Pulling    7m29s  kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     7m24s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 4.947566071s
  Normal  Created    7m24s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    7m24s  kubelet            Started container kubernetes-bootcamp


Name:         kubernetes-bootcamp-769746fd4-d47xm
Namespace:    default
Priority:     0
Node:         scale-worker2/172.23.0.3
Start Time:   Sat, 19 Jun 2021 15:49:54 +0900
Labels:       app=kubernetes-bootcamp
              pod-template-hash=769746fd4
Annotations:  <none>
Status:       Running
IP:           10.244.3.3
IPs:
  IP:           10.244.3.3
Controlled By:  ReplicaSet/kubernetes-bootcamp-769746fd4
Containers:
  kubernetes-bootcamp:
    Container ID:   containerd://c4514eb44a890e61e12eacaedbdc6456608bf055eb834405d67f102493b4974b
    Image:          jocatalin/kubernetes-bootcamp:v2
    Image ID:       docker.io/jocatalin/kubernetes-bootcamp@sha256:fb1a3ced00cecfc1f83f18ab5cd14199e30adc1b49aa4244f5d65ad3f5feb2a5
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Sat, 19 Jun 2021 15:50:00 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-tlwpw (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-tlwpw:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  7m23s  default-scheduler  Successfully assigned default/kubernetes-bootcamp-769746fd4-d47xm to scale-worker2
  Normal  Pulling    7m23s  kubelet            Pulling image "jocatalin/kubernetes-bootcamp:v2"
  Normal  Pulled     7m17s  kubelet            Successfully pulled image "jocatalin/kubernetes-bootcamp:v2" in 5.526844415s
  Normal  Created    7m17s  kubelet            Created container kubernetes-bootcamp
  Normal  Started    7m17s  kubelet            Started container kubernetes-bootcamp
```
