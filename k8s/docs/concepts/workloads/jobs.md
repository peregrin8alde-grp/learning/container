# Jobs

https://kubernetes.io/docs/concepts/workloads/controllers/job/

## Running an example Job

```
cat <<EOF | kubectl apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 4
EOF

$ kubectl describe jobs/pi
Name:           pi
Namespace:      default
Selector:       controller-uid=762ccefa-e056-42e1-a7e0-f53da6919740
Labels:         controller-uid=762ccefa-e056-42e1-a7e0-f53da6919740
                job-name=pi
Annotations:    <none>
Parallelism:    1
Completions:    1
Start Time:     Sat, 26 Jun 2021 13:58:58 +0900
Completed At:   Sat, 26 Jun 2021 13:59:46 +0900
Duration:       48s
Pods Statuses:  0 Running / 1 Succeeded / 0 Failed
Pod Template:
  Labels:  controller-uid=762ccefa-e056-42e1-a7e0-f53da6919740
           job-name=pi
  Containers:
   pi:
    Image:      perl
    Port:       <none>
    Host Port:  <none>
    Command:
      perl
      -Mbignum=bpi
      -wle
      print bpi(2000)
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From            Message
  ----    ------            ----  ----            -------
  Normal  SuccessfulCreate  54s   job-controller  Created pod: pi-gktpc
  Normal  Completed         6s    job-controller  Job completed

pods=$(kubectl get pods --selector=job-name=pi --output=jsonpath='{.items[*].metadata.name}')
echo $pods

kubectl logs $pods
```