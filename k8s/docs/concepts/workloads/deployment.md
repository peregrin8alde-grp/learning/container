# Deployment

https://kubernetes.io/ja/docs/concepts/workloads/controllers/deployment/

## Deploymentの作成

```
kubectl apply -f deployment.yaml
```

```
$ kubectl get deployments
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           17s

$ kubectl rollout status deployment.v1.apps/nginx-deployment
deployment "nginx-deployment" successfully rolled out

# ReplicaSet
$ kubectl get rs
NAME                          DESIRED   CURRENT   READY   AGE
nginx-deployment-66b6c48dd5   3         3         3       106s

# label
$ kubectl get pods --show-labels
NAME                                READY   STATUS    RESTARTS   AGE     LABELS
nginx-deployment-66b6c48dd5-2tsss   1/1     Running   0          2m47s   app=nginx,pod-template-hash=66b6c48dd5
nginx-deployment-66b6c48dd5-7vfwd   1/1     Running   0          2m47s   app=nginx,pod-template-hash=66b6c48dd5
nginx-deployment-66b6c48dd5-lh4qh   1/1     Running   0          2m47s   app=nginx,pod-template-hash=66b6c48dd5

$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Sun, 20 Jun 2021 18:14:16 +0900
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.14.2
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-66b6c48dd5 (3/3 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  3m48s  deployment-controller  Scaled up replica set nginx-deployment-66b6c48dd5 to 3
```

## Deploymentの更新

設定ファイル自体を書き換えて再度 `apply` するか、以下のいずれかで更新

```
kubectl --record deployment.apps/nginx-deployment set image deployment.v1.apps/nginx-deployment nginx=nginx:1.16.1

kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1 --record

# エディタで編集
kubectl edit deployment.v1.apps/nginx-deployment
```

```
kubectl rollout status deployment.v1.apps/nginx-deployment
```

```
$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Sun, 20 Jun 2021 18:14:16 +0900
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision: 2
                        kubernetes.io/change-cause: kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1 --record=true
Selector:               app=nginx
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.16.1
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-559d658b74 (3/3 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  8m19s  deployment-controller  Scaled up replica set nginx-deployment-66b6c48dd5 to 3
  Normal  ScalingReplicaSet  2m8s   deployment-controller  Scaled up replica set nginx-deployment-559d658b74 to 1
  Normal  ScalingReplicaSet  116s   deployment-controller  Scaled down replica set nginx-deployment-66b6c48dd5 to 2
  Normal  ScalingReplicaSet  116s   deployment-controller  Scaled up replica set nginx-deployment-559d658b74 to 2
  Normal  ScalingReplicaSet  104s   deployment-controller  Scaled down replica set nginx-deployment-66b6c48dd5 to 1
  Normal  ScalingReplicaSet  104s   deployment-controller  Scaled up replica set nginx-deployment-559d658b74 to 3
  Normal  ScalingReplicaSet  103s   deployment-controller  Scaled down replica set nginx-deployment-66b6c48dd5 to 0

$ kubectl get rs
NAME                          DESIRED   CURRENT   READY   AGE
nginx-deployment-559d658b74   3         3         3       2m51s
nginx-deployment-66b6c48dd5   0         0         0       9m2s

$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-559d658b74-gkrtt   1/1     Running   0          3m11s
nginx-deployment-559d658b74-nq8w9   1/1     Running   0          2m59s
nginx-deployment-559d658b74-z2jr8   1/1     Running   0          3m23s
```


## Deploymentの削除

```
kubectl delete deployment nginx-deployment
```
