# Secret

https://kubernetes.io/ja/docs/concepts/configuration/secret/

> KubernetesのSecretはパスワード、OAuthトークン、SSHキーのような機密情報を保存し、管理できるようにします。 Secretに機密情報を保存することは、それらをPodの定義やコンテナイメージに直接記載するより、安全で柔軟です。 詳しくはSecretの設計文書を参照してください。

> Secretはパスワード、トークン、キーのような小容量の機密データを含むオブジェクトです。 他の方法としては、そのような情報はPodの定義やイメージに含めることができます。 ユーザーはSecretを作ることができ、またシステムが作るSecretもあります。

## Secretの種類

### Opaque secrets

```
kubectl create secret generic empty-secret

$ kubectl get secret empty-secret
NAME           TYPE     DATA   AGE
empty-secret   Opaque   0      4s
```

### Service account token Secrets

### Docker config Secrets

### Basic authentication Secret

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-basic-auth
type: kubernetes.io/basic-auth
stringData:
  username: admin
  password: t0p-Secret
```

### SSH authentication secrets

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-ssh-auth
type: kubernetes.io/ssh-auth
data:
  # the data is abbreviated in this example
  ssh-privatekey: |
          MIIEpQIBAAKCAQEAulqb/Y ...
```

## ユースケース

### SSH鍵を持つPod

```
kubectl create secret generic ssh-key-secret --from-file=ssh-privatekey=$HOME/.ssh/id_rsa --from-file=ssh-publickey=$HOME/.ssh/id_rsa.pub
```

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: secret-test-pod
  labels:
    name: secret-test
spec:
  volumes:
  - name: secret-volume
    secret:
      secretName: ssh-key-secret
  containers:
  - name: ssh-test-container
    image: alpine
    command: ["sleep", "3600"]
    volumeMounts:
    - name: secret-volume
      readOnly: true
      mountPath: "/etc/secret-volume"
EOF

$ kubectl get secret ssh-key-secret
NAME             TYPE     DATA   AGE
ssh-key-secret   Opaque   2      5m20s

$ kubectl exec secret-test-pod -- ls /etc/secret-volume
ssh-privatekey
ssh-publickey

kubectl exec secret-test-pod -- cat /etc/secret-volume/ssh-privatekey
kubectl exec secret-test-pod -- cat /etc/secret-volume/ssh-publickey
```
