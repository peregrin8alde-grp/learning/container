# ConfigMap

https://kubernetes.io/ja/docs/concepts/configuration/configmap/

> ConfigMapは、 機密性のないデータをキーと値のペアで保存するために使用されるAPIオブジェクトです。Podは、環境変数、コマンドライン引数、またはボリューム内の設定ファイルとしてConfigMapを使用できます。

> ConfigMapを使用すると、環境固有の設定をコンテナイメージから分離できるため、アプリケーションを簡単に移植できるようになります。

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: game-demo
data:
  # プロパティーに似たキー。各キーは単純な値にマッピングされている
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.properties"

  # ファイルに似たキー
  game.properties: |
    enemy.types=aliens,monsters
    player.maximum-lives=5    
  user-interface.properties: |
    color.good=purple
    color.bad=yellow
    allow.textmode=true
EOF

kubectl apply -f yml/configmap-demo-pod.yml
```

```
kubectl get configmap game-demo
kubectl get pod configmap-demo-pod

kubectl describe configmap game-demo
kubectl describe pod configmap-demo-pod

$ kubectl exec configmap-demo-pod -- ls /config
game.properties
user-interface.properties

$ kubectl exec configmap-demo-pod -- cat /config/game.properties
enemy.types=aliens,monsters
player.maximum-lives=5
```
