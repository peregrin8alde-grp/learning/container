# Volumes

https://kubernetes.io/docs/concepts/storage/volumes/

## emptyDir

Pod が削除されるとディレクトリも削除される。

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
```

## hostPath

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # directory location on host
      path: /data
      # this field is optional
      type: Directory
```

```
apiVersion: v1
kind: Pod
metadata:
  name: test-webserver
spec:
  containers:
  - name: test-webserver
    image: k8s.gcr.io/test-webserver:latest
    volumeMounts:
    - mountPath: /var/local/aaa
      name: mydir
    - mountPath: /var/local/aaa/1.txt
      name: myfile
  volumes:
  - name: mydir
    hostPath:
      # Ensure the file directory is created.
      path: /var/local/aaa
      type: DirectoryOrCreate
  - name: myfile
    hostPath:
      path: /var/local/aaa/1.txt
      type: FileOrCreate
```

## local

hostPath と比べて、 k8s の管理下で扱われる。

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: example-pv
spec:
  capacity:
    storage: 100Gi
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /mnt/disks/ssd1
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - example-node
```

## nfs

Pod が削除されるとアンマウントされる。

https://github.com/kubernetes/examples/tree/master/staging/volumes/nfs

```
kubectl apply -f nfs/nfs-server.yml

$ kubectl get svc
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)                      AGE
kubernetes   ClusterIP   10.96.0.1     <none>        443/TCP                      33m
nfs-server   ClusterIP   10.96.127.8   <none>        2049/TCP,20048/TCP,111/TCP   28m

$ kubectl describe services nfs-server
Name:              nfs-server
Namespace:         default
Labels:            <none>
Annotations:       <none>
Selector:          role=nfs-server
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.96.127.8
IPs:               10.96.127.8
Port:              nfs  2049/TCP
TargetPort:        2049/TCP
Endpoints:         10.244.2.2:2049
Port:              mountd  20048/TCP
TargetPort:        20048/TCP
Endpoints:         10.244.2.2:20048
Port:              rpcbind  111/TCP
TargetPort:        111/TCP
Endpoints:         10.244.2.2:111
Session Affinity:  None
Events:            <none>
```

```
kubectl create -f nfs/nfs-pv.yml

$ kubectl get pv
NAME   CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
nfs    1Mi        RWX            Retain           Available                                   7s

$ kubectl describe pv nfs
Name:            nfs
Labels:          <none>
Annotations:     <none>
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:    
Status:          Available
Claim:           
Reclaim Policy:  Retain
Access Modes:    RWX
VolumeMode:      Filesystem
Capacity:        1Mi
Node Affinity:   <none>
Message:         
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    10.96.127.8
    Path:      /
    ReadOnly:  false
Events:        <none>

kubectl create -f nfs/nfs-pvc.yml

$ kubectl get pvc
NAME   STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
nfs    Bound    nfs      1Mi        RWX                           5s

$ kubectl describe pvc nfs
Name:          nfs
Namespace:     default
StorageClass:  
Status:        Bound
Volume:        nfs
Labels:        <none>
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      1Mi
Access Modes:  RWX
VolumeMode:    Filesystem
Used By:       <none>
Events:        <none>
```

```
kubectl apply -f nfs/nfs-busybox-rc.yml

$ kubectl get pod -l name=nfs-busybox
NAME                READY   STATUS    RESTARTS   AGE
nfs-busybox-7ptlc   1/1     Running   0          3s
nfs-busybox-l57xl   1/1     Running   0          3s

$ kubectl exec nfs-busybox-l57xl -- cat /mnt/index.html
Wed Jun 23 12:17:34 UTC 2021
nfs-busybox-7ptlc
```

## persistentVolumeClaim

管理者が用意した永続ボリュームを利用することでユーザは詳細な仕組み（クラウド基盤や iSCSI など）を意識する必要がない。
