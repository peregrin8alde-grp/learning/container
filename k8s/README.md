# Kubernetes

https://kubernetes.io/

https://kubernetes.io/ja/docs/home/

## kind

```
kind create cluster --config ./kind-config.yml

kind get clusters

kind delete cluster
```

## LoadBalancer

```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml

kubectl get pods -n metallb-system --watch
```

```
IP_BASE=$(docker network inspect -f '{{.IPAM.Config}}' kind | sed -E "s/\[\{([0-9]+\.[0-9]+)\.[0-9]+\..*/\1/g")

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - ${IP_BASE}.255.200-${IP_BASE}.255.250
EOF
```


## Ingress

### ingress-nginx

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s

$ kubectl get pods -n ingress-nginx
NAME                                       READY   STATUS      RESTARTS   AGE
ingress-nginx-admission-create-sjt8d       0/1     Completed   0          78s
ingress-nginx-admission-patch-8hdl9        0/1     Completed   0          78s
ingress-nginx-controller-744f97c4f-z6x4g   1/1     Running     0          79s

$ kubectl get svc -n ingress-nginx
NAME                                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx-controller             NodePort    10.96.117.16    <none>        80:31772/TCP,443:32221/TCP   76s
ingress-nginx-controller-admission   ClusterIP   10.96.163.100   <none>        443/TCP                      76s

$ kubectl get deployment -n ingress-nginx
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
ingress-nginx-controller   1/1     1            1           109s
```


```
kubectl logs ingress-nginx-controller-744f97c4f-z6x4g -n ingress-nginx
```

```
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
```

### Istio

```
istioctl install --set profile=demo -y

kubectl label namespace default istio-injection=enabled
```

## NFS

`volumes/nfs` 参照

## secret

```
kubectl create secret generic ssh-key-secret --from-file=ssh-privatekey=$HOME/.ssh/id_rsa --from-file=ssh-publickey=$HOME/.ssh/id_rsa.pub
```
