# Getting Started

https://istio.io/latest/docs/setup/getting-started/

## Download Istio

```
curl -L https://istio.io/downloadIstio | sh -

cd istio-1.10.2

###export PATH=$PWD/bin:$PATH
echo "export PATH=$PWD/bin:"'$PATH' >> ~/.profile
. ~/.profile
```

## Install Istio

- 詳細は [Install with Istioctl](https://istio.io/latest/docs/setup/install/istioctl/)
- `profile=demo` はデモ用に代表的な機能を一通りインストールする。詳細は [Installation Configuration Profiles](https://istio.io/latest/docs/setup/additional-setup/config-profiles/)
- `istio-injection=enabled` をラベルに設定することで対象の namespace に作られたアプリに自動で Envoy を作成
- アンインストールには `istioctl x uninstall --purge`
  - https://istio.io/latest/docs/setup/install/istioctl/#uninstall-istio

```
$ istioctl install --set profile=demo -y
✔ Istio core installed                                                                                                                                                                    
✔ Istiod installed                                                                                                                                                                        
✔ Egress gateways installed                                                                                                                                                               
✔ Ingress gateways installed                                                                                                                                                              
✔ Installation complete                                                                                                                                                                   Thank you for installing Istio 1.10.  Please take a few minutes to tell us about your install/upgrade experience!  https://forms.gle/KjkrDnMPByq7akrYA

kubectl label namespace default istio-injection=enabled
```

## Deploy the sample application

- 削除には `samples/bookinfo/platform/kube/cleanup.sh`

```
cd istio-1.10.2

kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml

$ kubectl get services
NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
details       ClusterIP   10.96.177.110   <none>        9080/TCP   12s
kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP    18m
productpage   ClusterIP   10.96.137.119   <none>        9080/TCP   11s
ratings       ClusterIP   10.96.185.253   <none>        9080/TCP   12s
reviews       ClusterIP   10.96.156.217   <none>        9080/TCP   12s

$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
details-v1-79f774bdb9-n4hzx       2/2     Running   0          2m18s
productpage-v1-6b746f74dc-wh7np   2/2     Running   0          2m17s
ratings-v1-b6994bb9-xp9pz         2/2     Running   0          2m18s
reviews-v1-545db77b95-mg2hr       2/2     Running   0          2m18s
reviews-v2-7bf8c9648f-w9rjw       2/2     Running   0          2m18s
reviews-v3-84779c7bbc-5d9kh       2/2     Running   0          2m17s

$ kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage | grep -o "<title>.*</title>"
<title>Simple Bookstore App</title>
```

## Open the application to outside traffic

```
$ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
gateway.networking.istio.io/bookinfo-gateway created
virtualservice.networking.istio.io/bookinfo created

$ istioctl analyze

✔ No validation issues found when analyzing namespace: default.
```

## Determining the ingress IP and ports

`istio-ingressgateway` が `LoadBalancer` のため、 LoadBalancer 未対応な環境では工夫が必要。


### LoadBalancer 対応環境

- kind の場合は Metallb を使う想定。 Docker のネットワーク上で IP を割り振る。

```
$ kubectl get svc istio-ingressgateway -n istio-system
NAME                   TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                                                                      AGE
istio-ingressgateway   LoadBalancer   10.96.167.142   172.23.255.201   15021:31790/TCP,80:31911/TCP,443:31825/TCP,31400:32631/TCP,15443:30199/TCP   7m57s
```

```
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].port}')

$ echo ${INGRESS_HOST}
172.23.255.201

$ echo ${INGRESS_PORT}
80

$ echo ${SECURE_INGRESS_PORT}
443

export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT

$ echo "$GATEWAY_URL"
172.23.255.201:80
```

```
$ echo "http://$GATEWAY_URL/productpage"
http://172.23.255.201:80/productpage
```

### LoadBalancer 未対応時

`NodePort` に変更など。

```
kubectl label nodes kind-worker ingressgateway=true

kubectl -n istio-system edit deployment istio-ingressgateway
```

```
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/arch
                operator: In
                values:
                - amd64
                - ppc64le
                - s390x
```

```
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: ingressgateway
                operator: In
                values:
                - "true"
```

```
$ kubectl -n istio-system get pods -o wide 
NAME                                    READY   STATUS    RESTARTS   AGE   IP           NODE           NOMINATED NODE   READINESS GATES
istio-egressgateway-7d4f75956-zrmxk     1/1     Running   0          25m   10.244.2.2   kind-worker    <none>           <none>
istio-ingressgateway-5d767d65f9-lmpb4   1/1     Running   0          50s   10.244.2.6   kind-worker    <none>           <none>
istiod-6f6c6bbbbd-sdhcd                 1/1     Running   0          26m   10.244.1.2   kind-worker2   <none>           <none>
```

```
kubectl -n istio-system edit service istio-ingressgateway
```

```
  - name: http2
    nodePort: 30152
    port: 80
    protocol: TCP
    targetPort: 8080
```

`nodePort` に kind でクラスタ作成した際に公開したポートを指定。 30000 台しか設定できない。

```
  - name: http2
    nodePort: 30080
    port: 80
    protocol: TCP
    targetPort: 8080
```

```
  type: LoadBalancer
```

```
  type: NodePort
```

```
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')

$ echo "$INGRESS_PORT"
30080

$ echo "$SECURE_INGRESS_PORT"
31919

###export INGRESS_HOST=$(minikube ip)
export INGRESS_HOST=localhost
$ echo "$INGRESS_HOST"
localhost

export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT

$ echo "$GATEWAY_URL"
localhost:30080

$ echo "http://$GATEWAY_URL/productpage"
http://localhost:30080/productpage
```

## View the dashboard

```
kubectl apply -f samples/addons
kubectl rollout status deployment/kiali -n istio-system

istioctl dashboard kiali
```

## Uninstall

```
samples/bookinfo/platform/kube/cleanup.sh

kubectl delete -f samples/addons
istioctl manifest generate --set profile=demo | kubectl delete --ignore-not-found=true -f -

kubectl delete namespace istio-system
kubectl label namespace default istio-injection-
```
