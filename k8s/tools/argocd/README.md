# Argo CD

https://argoproj.github.io/argo-cd/

## install

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl get -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

上手く起動しない場合、クラスタ再作成して余計なものを起動せずにやり直すと治る可能性あり。

```
VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')

sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64

sudo chmod +x /usr/local/bin/argocd
```

## Access The Argo CD API Server
### Ingress Configuration

https://argoproj.github.io/argo-cd/operator-manual/ingress/

#### Ambassador

試行錯誤中

```
kubectl apply -f routing.yml

argocd login localhost:80 --grpc-web-root-path /argo-cd
```

#### kubernetes/ingress-nginx

```
kubectl apply -f ingress-argocd.yml
```

kind + ingress での環境で log in が上手くいかず保留

=> argocd 自体の ingress は上手くいかないが、`kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission` などで
ingress をちゃんと削除してきれいな状態にしておけば、 Port Forwarding と他アプリの ingress は共存可能

### Port Forwarding

```
kubectl port-forward svc/argocd-server -n argocd 8080:443
```


## Log in

```
ARGO_PWD=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
echo ${ARGO_PWD}

argocd login localhost:8080
###argocd login localhost:80 --grpc-web-root-path /argo-cd --username admin --password ${ARGO_PWD} --insecure --plaintext

argocd account update-password
```

https://localhost:8080

https://localhost/argo-cd


## Register A Cluster To Deploy Apps To (Optional)

timeout が発生して上手くいかない。

外部クラスタに繋げる場合にのみ必要な作業のため、保留。
ArgoCD と同じクラスタの場合はデフォルトで選択できる `https://kubernetes.default.svc` を使えばいい。

```
$ kubectl config get-contexts -o name
kind-kind

$ argocd cluster add kind-kind
INFO[0000] ServiceAccount "argocd-manager" already exists in namespace "kube-system" 
INFO[0000] ClusterRole "argocd-manager-role" updated    
INFO[0000] ClusterRoleBinding "argocd-manager-role-binding" updated 


FATA[0020] Failed to establish connection to localhost:8080: context deadline exceeded 
```

