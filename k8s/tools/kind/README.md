# kind

https://kind.sigs.k8s.io/

## install

https://kind.sigs.k8s.io/docs/user/quick-start

```
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind
```

## Creating a Cluster

```
kind create cluster

kind create cluster --name kind-2
```

```
kind get clusters

kubectl cluster-info --context kind-kind
kubectl cluster-info --context kind-kind-2
```

## Deleting a Cluster

```
kind delete cluster

kind delete cluster --name kind-2
```

## Loading an Image Into Your Cluster

NOTE: You can get a list of images present on a cluster node by using docker exec

```
$ docker exec -it kind-control-plane crictl images
IMAGE                                      TAG                  IMAGE ID            SIZE
docker.io/kindest/kindnetd                 v20210326-1e038dc5   6de166512aa22       54MB
docker.io/rancher/local-path-provisioner   v0.0.14              e422121c9c5f9       13.4MB
k8s.gcr.io/build-image/debian-base         v2.1.0               c7c6c86897b63       21.1MB
k8s.gcr.io/coredns/coredns                 v1.8.0               296a6d5035e2d       12.9MB
k8s.gcr.io/etcd                            3.4.13-0             0369cf4303ffd       86.7MB
k8s.gcr.io/kube-apiserver                  v1.21.1              6401e478dcc01       127MB
k8s.gcr.io/kube-controller-manager         v1.21.1              d0d10a483067a       121MB
k8s.gcr.io/kube-proxy                      v1.21.1              ebd41ad8710f9       133MB
k8s.gcr.io/kube-scheduler                  v1.21.1              7813cf876a0d4       51.9MB
k8s.gcr.io/pause                           3.4.1                0f8457a4c2eca       301kB
```

## Configuring Your kind Cluster

```
$ kind create cluster --config kind-example-config.yaml

$ kubectl get nodes
NAME                 STATUS   ROLES                  AGE     VERSION
kind-control-plane   Ready    control-plane,master   7m45s   v1.21.1
kind-worker          Ready    <none>                 7m9s    v1.21.1
kind-worker2         Ready    <none>                 7m9s    v1.21.1
```
