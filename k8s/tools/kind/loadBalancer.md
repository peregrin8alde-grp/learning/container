# LoadBalancer

https://kind.sigs.k8s.io/docs/user/loadbalancer/

## Installing metallb using default manifests

```
# Create the metallb namespace
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml

# Create the memberlist secrets
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 

# Apply metallb manifest
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml

kubectl get pods -n metallb-system --watch
```

```
# Setup address pool used by loadbalancers
$ docker network inspect -f '{{.IPAM.Config}}' kind
[{172.23.0.0/16  172.23.0.1 map[]} {fc00:f853:ccd:e793::/64  fc00:f853:ccd:e793::1 map[]}]

IP_BASE=$(docker network inspect -f '{{.IPAM.Config}}' kind | sed -E "s/\[\{([0-9]+\.[0-9]+)\.[0-9]+\..*/\1/g")

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - ${IP_BASE}.255.200-${IP_BASE}.255.250
EOF
```

## Using LoadBalancer

```
cat <<EOF | kubectl apply -f -
kind: Pod
apiVersion: v1
metadata:
  name: foo-app
  labels:
    app: http-echo
spec:
  containers:
  - name: foo-app
    image: hashicorp/http-echo:0.2.3
    args:
    - "-text=foo"
---
kind: Pod
apiVersion: v1
metadata:
  name: bar-app
  labels:
    app: http-echo
spec:
  containers:
  - name: bar-app
    image: hashicorp/http-echo:0.2.3
    args:
    - "-text=bar"
---
kind: Service
apiVersion: v1
metadata:
  name: foo-service
spec:
  type: LoadBalancer
  selector:
    app: http-echo
  ports:
  # Default port used by the image
  - port: 5678
EOF
```

```
LB_IP=$(kubectl get svc/foo-service -o=jsonpath='{.status.loadBalancer.ingress[0].ip}')
$ echo ${LB_IP}
172.23.255.200

# should output foo and bar on separate lines 
for _ in {1..10}; do
  curl ${LB_IP}:5678
done

foo
bar
bar
bar
bar
foo
bar
bar
foo
foo
```
