# Ingress

https://kind.sigs.k8s.io/docs/user/ingress/

## Create Cluster

Create a kind cluster with extraPortMappings and node-labels.

- extraPortMappings allow the local host to make requests to the Ingress controller over ports 80/443
- node-labels only allow the ingress controller to run on a specific node(s) matching the label selector

```
cat <<EOF | kind create cluster --name ingress --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
EOF
```

## Deploy an Ingress controller

https://kubernetes.io/ja/docs/concepts/services-networking/ingress-controllers/

### Ambassador

https://www.getambassador.io/

```
kubectl apply -f https://github.com/datawire/ambassador-operator/releases/latest/download/ambassador-operator-crds.yaml
```

```
kubectl apply -n ambassador -f https://github.com/datawire/ambassador-operator/releases/latest/download/ambassador-operator-kind.yaml
kubectl wait --timeout=180s -n ambassador --for=condition=deployed ambassadorinstallations/ambassador

$ kubectl get -n ambassador all
NAME                                       READY   STATUS              RESTARTS   AGE
pod/ambassador-66f66cdd94-9qr9p            0/1     ContainerCreating   0          5s
pod/ambassador-agent-656467b9cf-ff65p      0/1     ContainerCreating   0          5s
pod/ambassador-operator-67668967b8-lkrtn   1/1     Running             0          34s

NAME                                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
service/ambassador                    NodePort    10.96.52.157    <none>        80:32708/TCP,443:30953/TCP   5s
service/ambassador-admin              ClusterIP   10.96.135.226   <none>        8877/TCP,8005/TCP            5s
service/ambassador-operator-metrics   ClusterIP   10.96.184.67    <none>        8383/TCP,8686/TCP            16s

NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ambassador            0/1     1            0           5s
deployment.apps/ambassador-agent      0/1     1            0           5s
deployment.apps/ambassador-operator   1/1     1            1           34s

NAME                                             DESIRED   CURRENT   READY   AGE
replicaset.apps/ambassador-66f66cdd94            1         1         0       5s
replicaset.apps/ambassador-agent-656467b9cf      1         1         0       5s
replicaset.apps/ambassador-operator-67668967b8   1         1         1       34s
```

`annotations` で `kubernetes.io/ingress.class: ambassador` をつけないと動かない。

例： `example-ingress` を `kubernetes.io/ingress.class: ambassador` つけずに作成して後から設定する場合

```
kubectl annotate ingress example-ingress kubernetes.io/ingress.class=ambassador
```

おそらく、最新の V2 ではなく 1.13 系なので仕様の違いに注意

- https://www.getambassador.io/docs/edge-stack/1.13/tutorials/getting-started/
- https://www.getambassador.io/docs/edge-stack/1.13/howtos/route/


### Contour

### Ingress NGINX

https://github.com/kubernetes/ingress-nginx/blob/master/README.md#readme

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
```

## Using Ingress

```
$ kubectl apply -f ingress/example.yml
pod/foo-app created
service/foo-service created
pod/bar-app created
service/bar-service created
Warning: networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.networking.k8s.io/example-ingress created
```

```
$ curl localhost/foo
foo

$ curl localhost/bar
bar
```

バージョンアップによる非推奨に対応（各種ドキュメントはまだそちらを利用していないため、 `v1beta1` を使った方が無難？）

https://kubernetes.io/docs/reference/using-api/deprecation-guide/#ingress-v122

- `networking.k8s.io/v1beta1` => `networking.k8s.io/v1`
- `spec.backend` is renamed to `spec.defaultBackend`
- The backend `serviceName` field is renamed to `service.name`
- Numeric backend `servicePort` fields are renamed to `service.port.number`
- String backend `servicePort` fields are renamed to `service.port.name`
- `pathType` is now required for each specified path. Options are `Prefix`, `Exact`, and `ImplementationSpecific`. To match the undefined `v1beta1` behavior, use `ImplementationSpecific`

