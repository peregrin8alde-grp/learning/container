# kubectl

- https://kubernetes.io/docs/reference/kubectl/
- https://kubernetes.io/ja/docs/reference/kubectl/_print/

## install

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

kubectl version --client
```

## using

### Objectの作成

```
kubectl apply -f ./my-manifest.yaml            # リソースを作成します
kubectl apply -f ./my1.yaml -f ./my2.yaml      # 複数のファイルからリソースを作成します
kubectl apply -f ./dir                         # dirディレクトリ内のすべてのマニフェストファイルからリソースを作成します
kubectl apply -f https://git.io/vPieo          # urlで公開されているファイルからリソースを作成します
kubectl create deployment nginx --image=nginx  # 単一のnginx Deploymentを作成します
kubectl explain pods                           # Podマニフェストのドキュメントを取得します

# 標準入力から複数のYAMLオブジェクトを作成します

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: busybox-sleep
spec:
  containers:
  - name: busybox
    image: busybox
    args:
    - sleep
    - "1000000"
---
apiVersion: v1
kind: Pod
metadata:
  name: busybox-sleep-less
spec:
  containers:
  - name: busybox
    image: busybox
    args:
    - sleep
    - "1000"
EOF

# いくつかの鍵を含むSecretを作成します

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  password: $(echo -n "s33msi4" | base64 -w0)
  username: $(echo -n "jane" | base64 -w0)
EOF
```

### shell

https://kubernetes.io/ja/docs/tasks/debug-application-cluster/get-shell-running-container/

```
$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
myapp-deployment-5c85b4d57f-kzhml   1/1     Running   1          21h
myapp-deployment-5c85b4d57f-pg29b   1/1     Running   1          21h
myapp-deployment-5c85b4d57f-x5x6l   1/1     Running   1          21h
```

```
kubectl exec -it myapp-deployment-5c85b4d57f-x5x6l -- /bin/bash

kubectl exec -i -t myapp-deployment-5c85b4d57f-x5x6l --container nginx -- /bin/bash
```

```
kubectl exec myapp-deployment-5c85b4d57f-x5x6l -- cat /usr/share/nginx/html/index.html

kubectl exec myapp-deployment-5c85b4d57f-x5x6l -- cat /etc/nginx/nginx.conf
```

```
kubectl exec -it deployment/myapp-deployment -- /bin/bash
```
