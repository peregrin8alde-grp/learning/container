# Web UI (Dashboard)

https://kubernetes.io/ja/docs/tasks/access-application-cluster/web-ui-dashboard/

## deploy

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
```

```
$ kubectl get pods --namespace kubernetes-dashboard
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-5594697f48-pkkwp   1/1     Running   0          7m24s
kubernetes-dashboard-57c9bfc8c8-h9zpl        1/1     Running   0          7m24s

$ kubectl get services --namespace kubernetes-dashboard
NAME                        TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
dashboard-metrics-scraper   ClusterIP   10.96.5.156    <none>        8000/TCP   7m48s
kubernetes-dashboard        ClusterIP   10.96.84.212   <none>        443/TCP    7m48s
```

## User

https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md

### Creating sample user

We are creating Service Account with name admin-user in namespace kubernetes-dashboard first.

```
kubectl apply -f dashboard-adminuser.yaml

kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}" > token.txt
```

Clean up

```
kubectl -n kubernetes-dashboard delete serviceaccount admin-user
kubectl -n kubernetes-dashboard delete clusterrolebinding admin-user
```

## use

```
kubectl proxy --address='0.0.0.0' --accept-hosts='.*' &
```

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
