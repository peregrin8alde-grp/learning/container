# アプリ実行

```
kubectl apply -f gateway.yml

export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].port}')
```

```
kubectl apply -f myapp01

kubectl get pods --watch

echo http://${INGRESS_HOST}/myapp01
```

```
kubectl apply -f theia

kubectl get pods --watch

echo http://${INGRESS_HOST}/theia/
```

