# トラブルシューティング

- `Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": an error on the server ("") has prevented the request from succeeding`
  - `kubectl logs ingress-nginx-controller-744f97c4f-z6x4g -n ingress-nginx`
    - `"failed to process webhook request" err="rejecting admission review because the request does not contain an Ingress resource but networking.k8s.io/v1, Kind=Ingress with name myapp01-ingress in namespace default"`
  - https://stackoverflow.com/questions/61365202/nginx-ingress-service-ingress-nginx-controller-admission-not-found

        $ kubectl get -A ValidatingWebhookConfiguration
        NAME                      WEBHOOKS   AGE
        ingress-nginx-admission   1          17m

  - `kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission` を実行すると解決した。
- Ingress 経由でアプリにアクセスした場合に、 nodejs のアプリ（ theia ）で js / css などのリソースが見つからない。
  - rewrite はそこまで対応してくれないため、アプリ側でベース URL などを設定する必要がある？
  - `configuration-snippet` を使って nginx の設定を追加

        nginx.ingress.kubernetes.io/configuration-snippet: |
          rewrite ^(/theia)$ $1/ redirect;

- `GET ws://localhost/theia/services` で 403

        apiVersion: getambassador.io/v2
        kind:  Mapping
        metadata:
          name:  theia-mapping
        spec:
          prefix: /theia/
          service: theia-service:3000

- nginx が何らかの要因で停止しないせいで クラスタやコンテナが停止／削除できない。
  - `kind delete cluster` や OS シャットダウンでは発生するが、 `kubectl delete` では発生しない。
  - NFS を k8s 上で実行して利用しているせいで、ディスクアクセス関連の停止順が問題になった？
  - NFS サーバをクラスタと分けることで解決


