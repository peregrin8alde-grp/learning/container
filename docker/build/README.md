# docker build

https://docs.docker.com/engine/reference/commandline/build/

## 基本


## よく利用するオプション

```
docker build --rm -t tmp --force-rm=true .
```

試行錯誤時にはイメージ自動削除

```
--force-rm
```

